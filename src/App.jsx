import { MainCard } from './component/mainCard'
import { Competitions } from './component/Competitions'
import { WantToLearn } from './component/WantToLearn'
import './App.css'
import './component/mainCard.css'
import './component/Competitions.css'
import './component/WantToLearn.css'


function App() {
  return (
    <>
    <MainCard author={author}/>
    <Competitions compfirst={compfirst} secondcomp={secondcomp} thirdcomp={thirdcomp} fourthcomp={fourthcomp}/>
    <WantToLearn wanttolearn={wanttolearn}/>
    </>
  )
}

const author = 'Yusif Gurbanov'

const wanttolearn = ['TypeScript', 'PostgreSQL', 'knex', 'Docker', 'Node.js']
  
const compfirst = {
  language: 'Java',
  framework: 'Spring'
}
const secondcomp = {
  language: 'Kotlin',
  framework: 'Spring Boot',
  frontend: 'Jetpack Compose'
}
const thirdcomp = {
  skill: 'Frontend',
  markuplanguage: 'HTML CSS',
  language: 'JavaScript',
  library: 'React'
}
const fourthcomp = {
  skill: 'Basketball',
  practising: '7 years',
  team: 'Los Angeles Lakers , Milwaukee Bucks'
}

export default App