export function WantToLearn (props) {
    return (
        <>
        <div className="WantToLearn">
            <h1>Want to learn skills</h1>
        <ul className="wanttolearn">
            {props.wanttolearn.map(el => <li key={el}>{el}</li>)}
        </ul>
        </div>
        </>
    )
}

export default WantToLearn