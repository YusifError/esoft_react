export function Competitions ({compfirst, secondcomp, thirdcomp, fourthcomp}) {
        return (
            <>
            <div className="firstComp">
            <h1>{compfirst.language}</h1>
            <p>{compfirst.framework}</p>
            </div>

            <div className="secondComp">
            <h1>{secondcomp.language}<br /></h1>
            <p>{secondcomp.framework}<br /></p>
            <p>{secondcomp.frontend}<br /></p>
            </div>

            <div className="thirdcomp">
            <h1>{thirdcomp.skill}</h1>
            <p>{thirdcomp.markuplanguage}</p>
            <p>{thirdcomp.style}</p>
            <p>{thirdcomp.language}</p>
            <p>{thirdcomp.library}</p>
            </div>

            <div className="fourthcomp">
            <h1>{fourthcomp.skill}</h1>
            <p>{fourthcomp.practising}</p>
            <p>{fourthcomp.team}</p>
            </div>
            </>
        )
}

export default Competitions